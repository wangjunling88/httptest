package com.xizhen.test.Dao;

import com.xizhen.test.entity.Product;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品信息持久层接口
 */
@Mapper
public interface ProductMapper {

    Product selectByPrimaryKey(Integer id);
}
