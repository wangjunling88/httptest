package com.xizhen.test.service;

import com.xizhen.test.entity.Product;

public interface IProductService {
    Product selectByPrimaryKey(Integer id);
}
