package com.xizhen.test.service.impl;

import com.xizhen.test.Dao.ProductMapper;
import com.xizhen.test.entity.Product;
import com.xizhen.test.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 产品信息业务类
 */
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    /**
     * 根据产品id查询产品信息
     * @param id
     * @return Product
     */
    @Override
    public Product selectByPrimaryKey(Integer id) {
        return productMapper.selectByPrimaryKey(id);
    }
}
