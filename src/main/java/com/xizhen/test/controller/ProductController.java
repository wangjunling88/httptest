package com.xizhen.test.controller;
import com.xizhen.test.entity.Product;
import com.xizhen.test.service.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品信息控制器
 */
@RestController
@Slf4j
public class ProductController {
    @Autowired
    private IProductService iProductService;

    /**
     * 根据产品Id查询产品信息控制器
     * @return Product
     */
    @PostMapping("/selectProductById")
    public Product selectProductById(){
        Product product = iProductService.selectByPrimaryKey(1);
        log.info("查询到的产品信息为:{}",product);
        return product;
    }


}
