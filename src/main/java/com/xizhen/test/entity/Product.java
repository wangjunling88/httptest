package com.xizhen.test.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 产品信息实体类
 */
@Data
public class Product {
    private Integer id;//产品主键
    private String productName;//产品名称
    private BigDecimal productPrice;//产品价格
    private String productDesc;//产品描述
}
