# httpclienttest-server

#### 介绍
   本项目为服务端模拟接口提供项目，实现了下面需求中的模拟接口提供。
   并行且异步发送n个http post请求的案例，当n个请求都有返回，
并且在各自的时间内获得response,要求是第一个request在发出去的1秒
内收到，第二个request在2秒内.....第n个request在n秒内收到，则返回
成功;如果不满足以上条件且等待超过n秒，则返回失败，并且记录失败的
原因;支持打印输出到log文件，并以清晰的格式呈现;提供必要的http端服
务程序，能够接收http request 以及模拟所需要的行为;




#### 软件架构
软件架构说明

本项目为模拟接口提供服务端，技术栈整体采用springboot、mybatis、springmvc、jdk1.8、maven等。
#### 安装教程

1.  本项目可以打成jar包或war包发布到服务器运行
2.  xxxx
3.  xxxx

#### 使用说明

1. 本项目提供了一个（根据产品Id查询产品信息控制器接口）服务端模拟接口，可以t通过postman、httpclient等工具进行调用测试。
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
